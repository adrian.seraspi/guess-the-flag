//
//  ContentView.swift
//  Guess The Flag
//
//  Created by Adrian Jun Seraspi on 4/24/24.
//

import SwiftUI

struct ContentView: View {
  @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Spain", "UK", "Ukraine", "US"].shuffled()
  @State private var correctAnswer = Int.random(in: 0...2)
  
  @State private var showingScore = false
  @State private var showingTotal = false
  
  @State private var scoreTitle = ""
  @State private var currentScore = 0
  @State private var questionIndex = 1
  
  var body: some View {
    ZStack {
      RadialGradient(
        stops: [
          .init(color: Color(red: 0.1, green: 0.2, blue: 0.45), location: 0.3),
          .init(color: Color(red: 0.76, green: 0.15, blue: 0.26), location: 0.3)
        ],
        center: .top,
        startRadius: 200,
        endRadius: 700
      )
      .ignoresSafeArea()
      
      VStack {
        Spacer()
        
        Text("Guess the Flag")
          .font(.largeTitle.bold())
          .foregroundStyle(.white)
        
        VStack(spacing: 15) {
          VStack {
            Text("Tap the flag of")
              .font(.subheadline.weight(.semibold))
              .foregroundStyle(.secondary)
            Text(correctAnswerText)
              .font(.largeTitle.weight(.heavy))
          }
          
          ForEach(0..<3) { number in
            Button {
              flagTapped(number)
            } label: {
              Image(countries[number])
                .clipShape(.capsule)
                .shadow(radius: 5)
            }
            .alert(scoreTitle, isPresented: $showingScore) {
              Button("Continue", action: askQuestion)
            } message: {
              Text("Your score is \(currentScore)")
            }
            .alert("Game Completed!", isPresented: $showingTotal) {
              Button("Start", action: reset)
            } message: {
              Text("Your score is \(currentScore)")
            }
          }
        }
        .frame(maxWidth: .infinity)
        .padding(.vertical, 20)
        .background(.regularMaterial)
        .clipShape(.rect(cornerRadius: 20))
        
        Spacer()
        
        Text("Score: \(currentScore)")
          .foregroundStyle(.white)
          .font(.title.bold())
        
        Spacer()
      }
      .padding()
    }
  }
}

// MARK: - Helpers

private extension ContentView {
  func flagTapped(_ number: Int) {
    if number == correctAnswer {
      scoreTitle = "Correct"
      currentScore += 1
    } else {
      scoreTitle = "Wrong! That's the flag of \(correctAnswerText)"
    }
    
    guard !hasCompletedQuestion else {
      showingTotal = true
      return
    }
    
    showingScore = true
    questionIndex += 1
  }
  
  func askQuestion() {
    countries.shuffle()
    correctAnswer = Int.random(in: 0...2)
  }
  
  func reset() {
    questionIndex = 1
    currentScore = 0
    
    askQuestion()
  }
}

// MARK: - Getters

private extension ContentView {
  var correctAnswerText: String {
    countries[correctAnswer]
  }
  
  var hasCompletedQuestion: Bool {
    questionIndex == 8
  }
}

#Preview {
  ContentView()
}
