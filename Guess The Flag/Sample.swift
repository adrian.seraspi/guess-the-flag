//
//  ContentView.swift
//  Guess The Flag
//
//  Created by Adrian Jun Seraspi on 4/24/24.
//

import SwiftUI

struct Sample: View {
    @State private var showingAlert = false
    
    var body: some View {
        ZStack {
            VStack(spacing: 0) {
                Color.red
                Color.blue
            }
            
            Text("Your content")
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .foregroundStyle(.secondary)
            
            VStack {
                Button {
                    showingAlert = true
                } label: {
                    Text("Show Alert")
                        .padding()
                        .foregroundStyle(.white)
                        .background(.blue)
                }.alert("Important message", isPresented: $showingAlert) {
                    Button("OK") {}
                    Button("Delete", role: .destructive) {}
                    Button("Cancel", role: .cancel) {}
                } message: {
                    Text("Please read this.")
                }
                
                Button {
                    print("Button was tapped")
                } label: {
                    Label("Edit", systemImage: "pencil")
                        .padding()
                        .foregroundStyle(.white)
                        .background(.blue)
                }
                
                Button("Button 1") {
                    
                }
                .buttonStyle(.bordered)
                Button("Button 2", role: .destructive) {
                    
                }
                .buttonStyle(.bordered)
                
                Button("Button 3") {
                    
                }
                .buttonStyle(.borderedProminent)
                .tint(.mint)
                
                Button("Button 4", role: .destructive) {
                    
                }
                .buttonStyle(.borderedProminent)
                
            }
        }
        .ignoresSafeArea()
    }
}

#Preview {
    ContentView()
}
